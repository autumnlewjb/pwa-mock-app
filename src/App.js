import React, { useState, useRef } from 'react';
import './App.css';
import { Typography, Container, Stack, Paper, Button, Box, TextField, Dialog, DialogContent, DialogContentText, DialogActions } from '@mui/material';
import { useEffect } from 'react';

function App() {
  const [data, setData] = useState([]);
  const [comment, setComment] = useState("");
  const [openDialog, setOpenDialog] = useState(true);
  const [username, setUsername] = useState("");
  const [reloading, setReloading] = useState();

  const [pendingReq, setPendingReq] = useState([]);


  useEffect(() => {
    reloadData();
  }, [])

  useEffect(() => {
    const timeoutFunc = () => setTimeout(reloadData, 5000)
    window.addEventListener('online', timeoutFunc);

    return () => {
      window.removeEventListener('online', timeoutFunc)
    }
  }, [])

  const reloadData = () => {
    setReloading(true)
    fetch(`${process.env.REACT_APP_API_URL}/comments`)
      .then((res) => res.json())
      .then(data => setData(data))
      .then(() => {
        setReloading(false);
        setPendingReq([]);
      })
      .catch(() => setReloading(false));
  }

  const handleSubmit = () => {
    const body = {
      text: comment,
      writtenBy: username,
    }
    fetch(`${process.env.REACT_APP_API_URL}/comments`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    })
      .then(() => {
        reloadData();
      })
      .catch(err => {
        if (err.message === "Failed to fetch") {
          setPendingReq((pendingReq) => [...pendingReq, body]);
        }
      })
    setComment("");
  }

  const handleUsernameConfirm = () => {
    setOpenDialog(false);
  }

  return (
    <div className="App">
      <Container sx={{ padding: 5, height: '100vh', width: "100vw" }} >
        <Box display="flex">
          <Box display="flex" sx={{ marginBottom: 5, flexGrow: 5, padding: 3 }}>
            <Typography variant='h4'>Do you think that PWA is useful?</Typography>
          </Box>
          <Box sx={{ flexGrow: 1, alignSelf: "center" }}>
            <Button onClick={reloadData} disabled={reloading}>{reloading ? "Reloading" : "Reload"}</Button>
          </Box>
        </Box>
        <Box display="flex" sx={{ marginBottom: 3, minHeight: '70%', overflow: 'auto', maxHeight: "70%", flexDirection: 'column-reverse' }}>
          <Stack>
            {data.map(comment => {
              return <Paper sx={{ textAlign: "left", padding: 5, marginBottom: 2 }}>
                <Typography variant='overline'>{comment.writtenBy}</Typography>
                <Typography variant='body1' sx={{ whiteSpace: 'pre-line' }}>
                  {comment.text}
                </Typography>
                <Typography variant='caption' sx={{ textAlign: 'right' }}>{new Date(comment.createdAt).toString()}</Typography>
              </Paper>
            })}
            {pendingReq.map(reqBody => {
              return <Paper sx={{ textAlign: "left", padding: 5, marginBottom: 2 }}>
                <Typography variant='overline'>{reqBody.writtenBy}</Typography>
                <Typography variant='body1' sx={{ whiteSpace: 'pre-line' }}>
                  {reqBody.text}
                </Typography>
                <Typography variant='caption' sx={{ textAlign: 'right' }}>Posting...</Typography>
              </Paper>
            })}
          </Stack>
        </Box>
        <Box display="flex">
          <Box sx={{ flexGrow: 2 }}>
            <TextField multiline fullWidth onChange={(e) => setComment(e.target.value)} value={comment}></TextField>
          </Box>
          <Box sx={{ alignSelf: 'center', margin: 1 }}>
            <Button onClick={handleSubmit}>Post</Button>
          </Box>
        </Box>
      </Container>
      <Dialog open={openDialog} onClose={() => { }} onBackdropClick={false}>
        <DialogContent>
          <DialogContentText>
            Enter your username for your comments.
          </DialogContentText>
          <TextField autoFocus fullWidth value={username} onChange={(e) => setUsername(e.target.value)} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleUsernameConfirm}>Confirm</Button>
        </DialogActions>
      </Dialog>
    </div >
  );
}

export default App;
